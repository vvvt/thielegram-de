import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const Art = () => (
  <Layout>
    <SEO title="Art" />
    <h1>Art</h1>
    <p>With nearly a decade of experience in digital design and image manipulation, I went on several creative endeavours,
    most notably designing official promotional material for <a
        href="https://werkstattorchester.de">Werkstattorchester Dresden e.V.</a> and <a
          href="https://www.curiegymnasium.goerlitz.de/">Joliot-Curie-Gymnasium Görlitz</a>
    </p>
    <h2>Posters &amp; Flyers</h2>
    <p>Every event needs to be advertised and every advertisement needs someone to create effective and eye-catching
        promotional material. In cooperation with different illustrators and by utilizing various styles, we lifted a number
    of ideas from the think tank and turned them into striking posters.</p>
    <ul>
      <li><span className="portfolio-entry">Werkstattorchester Dresden e.V. - Alte Welt | Neue Welt</span><span className="portfolio-year">2019</span></li>
      <li><span className="portfolio-entry">Werkstattorchester Dresden e.V. - tea time</span><span className="portfolio-year">2019</span></li>
      <li><span className="portfolio-entry">Werkstattorchester Dresden e.V. - Orff Beatz</span><span className="portfolio-year">2018</span></li>
      <li><span className="portfolio-entry">Werkstattorchester Dresden e.V. - Romantischer wird's nicht</span><span className="portfolio-year">2018</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - official school flyer</span><span className="portfolio-year">2015</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - Halloweenbasar</span><span className="portfolio-year">2014</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - Frühstücksbasar</span><span className="portfolio-year">2014</span></li>
    </ul>
    <h2>Logos &amp; Icons</h2>
    <p>In the current day and age, a good logo can speak more than a thousand words.</p>
    <ul>
      <li><span className="portfolio-entry">Vampir - in-app icons</span><span className="portfolio-year">since 2019</span></li>
    </ul>
    <h2>Miscellaneous</h2>
    <p>Besides the previous works, every so often there are these creative endeavours that don't fit any of the given
    categories.</p>
    <ul>
      <li><span className="portfolio-entry">Kinderhaus Tausendfuß - advisors board member overview</span><span className="portfolio-year">2017</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - graduation shirt</span><span className="portfolio-year">2015</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - graduation paper</span><span className="portfolio-year">2015</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - offical graduation artwork</span><span className="portfolio-year">2015</span></li>
      <li><span className="portfolio-entry">Joliot-Curie-Gymnasium - official planner artwork</span><span className="portfolio-year">2014</span></li>
    </ul>
  </Layout>
)

export default Art
