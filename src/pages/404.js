import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1>Oh snap!</h1>
    <p>The page you were looking for isn't here.</p>
  </Layout>
)

export default NotFoundPage
