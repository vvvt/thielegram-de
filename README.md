# thielegram.de

Personal website, giving a short overview of my activities and interests both past and present.

## Projects & Assignments

Studying in the field of computer science comes with a variety of assignments and side projects exercising and testing out the new developed skills.

## Music & Composition

While training in the field of computer science, I remained a musician at heart, enjoying the virtues of playing solo and ensemble for almost 20 years. While the warm and mysterious bassoon has been my main instrument for the past 14 years, I picked up playing the guitar as a passion project about 6 years ago.

## Art & Design

With almost a decade of experience in digital design and photo editing, I went on several creative endeavours, most notably designing official promotional material for [Werkstattorchester Dresden e.V.](https://werkstattorchester.de) and [Joliot-Curie-Gymnasium Görlitz](https://www.curiegymnasium.goerlitz.de/). 
